#! /bin/bash
#
#▄   ▄             ▗▖▗▄▖             ▗▄▖      ▗▄▖
#█   █             ▐▌▝▜▌            ▗▛▀▜      ▝▜▌
#▜▖█▗▛ ▟█▙  █▟█▌ ▟█▟▌ ▐▌   ▟█▙      ▐▙    ▟█▙  ▐▌  ▐▙ ▟▌ ▟█▙  █▟█▌
#▐▌█▐▌▐▛ ▜▌ █▘  ▐▛ ▜▌ ▐▌  ▐▙▄▟▌      ▜█▙ ▐▛ ▜▌ ▐▌   █ █ ▐▙▄▟▌ █▘
#▐█▀█▌▐▌ ▐▌ █   ▐▌ ▐▌ ▐▌  ▐▛▀▀▘ ██▌    ▜▌▐▌ ▐▌ ▐▌   ▜▄▛ ▐▛▀▀▘ █
#▐█ █▌▝█▄█▘ █   ▝█▄█▌ ▐▙▄ ▝█▄▄▌     ▐▄▄▟▘▝█▄█▘ ▐▙▄  ▐█▌ ▝█▄▄▌ █
#▝▀ ▀▘ ▝▀▘  ▀    ▝▀▝▘  ▀▀  ▝▀▀       ▀▀▘  ▝▀▘   ▀▀   ▀   ▝▀▀  ▀
#
# written by Christos Angelopoulos, June 2022
function select_word {
	w=$(look .| eval $FINAL_STRING|grep -v "'"|shuf|head -1|sed  's/\(.*\)/\L\1/')
	WORDCOUNT=$(look .| eval $FINAL_STRING|grep -v "'"|sed  's/\(.*\)/\L\1/'|uniq|wc -l)
}
Yellow="\033[1;33m"
bold=`tput bold`
normal=`tput sgr0`
replay=y
clear
FINAL_GREP_E_STRING="grep -E \"^.....$\""
WIN=0
for try in {1..6}
do
	FINAL_STRING="$FINAL_GREP_E_STRING""$FINAL_GREP_VE_STRING""$CORRECT_STRING""$MISPLACED_STRING""$ABSENT_STRING"
	db="c"
	select_word
	while [[ $db = "c" ]]
	do

		if [ $WORDCOUNT -eq 1 ]
		then
			echo  "╭──────────────────────────────────────╮"
			echo -e "│ Wordle Solved!                       │\n│The word was ${Yellow}${bold}\"$w\"${normal}.                 │"
			echo  "╰──────────────────────────────────────╯"
			echo -en "Goodbye! " ;read -N 1 d
			clear;exit
	fi
		echo -e "Possible solutions : ${Yellow}${bold}$WORDCOUNT${normal}"
		echo "╭──────────────────────────────────────╮"
		echo -e "│Enter the word : ${Yellow}${bold}"$w" ${normal}               │"
		echo  "├──────────────────────────────────────┤"
		echo -e "│Enter:                                │\n│ ${Yellow}${bold}c${normal} to select a different word.        │\n│ ${Yellow}${bold}e ${normal}to enter a word you prefer.        │\n│ ${Yellow}${bold}p${normal} to list all possible words.        │\n│ ${Yellow}${bold}Enter${normal} to proceed.                    │\n│ ${Yellow}${bold}q ${normal}to quit.                           │"
		echo  "╰──────────────────────────────────────╯"
		echo -en "Select: ";read -N 1  db
		case $db in
			p) clear;look .| eval $FINAL_STRING|grep -v "'"|sed  's/\(.*\)/\L\1/'|uniq;db=c
			;;
			e) clear;echo -e "Enter your ${Yellow}${bold}5 letter word ${normal}:";read  w; if [ $(echo $w|wc -m) -ne 6 ];then db=c; echo -e "You should enter ${Yellow}${bold}a 5 letter word.${normal}";select_word;fi
			;;
			q)clear;exit
			;;
			c)clear;select_word
			;;
			$'\x0a')clear
			;;
			*)clear;echo "Invalid key pressed, try again.";db=c
		esac
	done
	clear
	for i in {0..4}
	do

		GREP_E_STRING[$i]=""
		for ii in {0..4}
		do
			if [ $ii -eq $i ]
			then
				DIGIT="${w:i:1}"
			else
				DIGIT="."
			fi
			GREP_E_STRING[$i]=${GREP_E_STRING[$i]}"$DIGIT"
		done

		if [[  ${POS[@]} != *$i* ]]
		then
			echo -e "╭──────────────────────────────────────╮\n│ ${Yellow}${bold}${w:i:1}${normal} in position ${Yellow}${bold}$(($i + 1)) ${normal}is:                  │\n│ ${Yellow}${bold} 1${normal}.Correct       ${Yellow}${bold} 2${normal}.Misplaced        │\n│ ${Yellow}${bold} 3${normal}.Absent        ${Yellow}${bold} 4${normal}.Quit             │\n╰──────────────────────────────────────╯"
			db1=1
			while [ $db1 -eq 1 ]
			do
				echo -en "Select : "; read -N 1 l[$i]
				case ${l[$i]} in
					1)  echo -e ". Letter ${Yellow}${bold} ${w:i:1}${normal} is CORRECT          ";CORRECT+=("${w:i:1}");WORD[$i]=${w:i:1};GREP_E_STRING[$i]="grep -E \"${GREP_E_STRING[$i]}\"";FINAL_GREP_E_STRING+=\|${GREP_E_STRING[$i]};POS[$i]=$i;if [[ ${ABSENT[@]} == *${w:i:1}* ]];then ABSENT=( ${ABSENT[@]/${w:i:1}/} ) ;fi;db1=0
					;;
					2) echo -e ". Letter ${Yellow}${bold}${w:i:1}${normal} is MISPLACED         ";MISPLACED+=("${w:i:1}");GREP_VE_STRING[$i]="grep -vE \"${GREP_E_STRING[$i]}\"";FINAL_GREP_VE_STRING+=\|${GREP_VE_STRING[$i]};db1=0
					;;
					3) echo -e ". Letter ${Yellow}${bold}${w:i:1} ${normal}is ABSENT            ";if [[ ${ABSENT[@]} != *${w:i:1}* ]] && [[ ${CORRECT[@]} != *${w:i:1}* ]] && [[ ${MISPLACED[@]} != *${w:i:1}* ]]  ; then ABSENT+=("${w:i:1}");fi;db1=0
					;;
					4)clear;exit
					;;
					*) echo ". Invalid key pressed, try again."
					;;
				esac
			done
		fi
	done
	clear
	echo "╭──────────────────────────────────────╮"
	echo -e "│Correct letters :${Yellow}${bold} ${#POS[@]}${normal}                   │"
	echo  "╰──────────────────────────────────────╯"
	for c in "${CORRECT[@]}"
	do
		CORRECT_STRING=$CORRECT_STRING"|grep "$c
	done
	for m in "${MISPLACED[@]}"
	do
		MISPLACED_STRING=$MISPLACED_STRING"|grep "$m
	done
	for a in "${ABSENT[@]}"
	do
		ABSENT_STRING=$ABSENT_STRING"|grep -v "$a
	done
done
