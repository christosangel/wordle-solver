# Wordle Solver

A bash script that can solve wordle riddles.

## SCREENSHOTS

* Main menu

![0.png](0.png)

* Entering letter info

![1.png](1.png)

* Success!

![2.png](2.png)

* Statistics

![stats.png](stats.png)
